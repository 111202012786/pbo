public class Pegawai extends Manusia {
    public String nik;
    public String department;
    public boolean is_report_created;
    public boolean off_to_work;
    public boolean home_from_work;

    public Pegawai(String name, int age, int height, String nik, String department) {
        super(name, age, height);
        this.nik = nik;
        this.department = department;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setIsReportCreated(boolean is_report_created) {
        this.is_report_created = is_report_created;
    }

    public void getNik() {
        System.out.println("My nik is " + nik);
    }

    public void getDepartment() {
        System.out.println("I'm working in " + department);
    }

    public void setHomeFromWork(boolean home_from_work) {
        this.home_from_work = home_from_work;
    }

    public void setOffToWork(boolean off_to_work) {
        this.off_to_work = off_to_work;
    }

    public void getOffToWork() {
        if (off_to_work) {
            System.out.println("I'm off to work");
        } else {
            System.out.println("I'm not off to work");
        }
    }

    public void getIsReportCreated() {
        if (is_report_created) {
            System.out.println("I'm reporting");
        } else {
            System.out.println("I'm not reporting");
        }
    }

    public void getHomeFromWork() {
        if (home_from_work) {
            System.out.println("I have come home from work");
        } else {
            System.out.println("saya belum pulang kerja");
        }
    }
}
