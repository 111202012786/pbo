public class Main {
    public static void main(String[] args) {
        System.out.println("Data Mahasiswa: ");
        Mahasiswa mahasiswa = new Mahasiswa("Rizki", 20, 170, "123456789", 3.5);
        mahasiswa.getBiodata();

        System.out.println("\nData Pegawai: ");
        Pegawai pegawai = new Pegawai("Rizky", 20, 170, "123456789", "IT");
        pegawai.getName();
        pegawai.getAge();
        pegawai.getHeight();
        pegawai.getNik();
        pegawai.getDepartment();
        pegawai.setIsReportCreated(true);
        pegawai.getIsReportCreated();
    }
}
