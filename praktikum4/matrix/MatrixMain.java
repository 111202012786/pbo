public class MatrixMain {
	
	public static void main(String[] args) { 
		double[][] d = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
		Matrix D = new Matrix(d);
		double[][] a = { { 1, 2, 4}, {5, 6, 8}, {9, 0, 2}};
		double[][] c = { { 1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
		Matrix A = new Matrix(a);
		Matrix B = A.transpose();
		Matrix C = new Matrix(c);
		
		C.show();
		D.show(); 
		
		System.out.println();
		A.swap(1, 2);
		A.show(); 
		System.out.println();
		
		// shouldn't be equal since AB != BA in general 
		System.out.println(A.times(B).eq(B.times(A)));
		System.out.println();
		Matrix b = Matrix.random(3, 1);
		b.show();
		
		System.out.println();
		Matrix x = A.solve(b);
		x.show();
		
		System.out.println();
		A.times(x).show();	 
	}
}
