public class AksiSepeda {
	
	public static void main(String[] args) {
		// membuat objek
		Sepeda sepedaBalap = new Sepeda(2, "Polygon","BMX");
		Sepeda sepedaAnak = new Sepeda(3, "Mini12","Family");
			
		int gearSepeda = sepedaBalap.gear;
		System.out.println(gearSepeda);
		sepedaBalap.ngerem();
		
		int anakGearSepeda = sepedaAnak.gear;
		System.out.println(anakGearSepeda);
		sepedaAnak.ngerem();
	}
	
}
