package pbo1.calc;

public class RightTriangleArea {
    public static void main(String[] args) {
        double base = 6, height = 8, area;

        area = 0.5 * base * height;
        System.out.println("Hasil: " + String.format("%.2f", area));
    }
}